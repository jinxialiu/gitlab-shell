#类的开始引入了在该类中可能用到的其他类（应该是），require_relative不知道意思
#首先是一个主类，从类的名称上看，是gitalb关于access控制的类
#类中先是包含一个内部类，该类继承自标准错误类，从名称上看是处理权限被拒绝的类，且没有实际类主体。
#include包含一个helper，未知用途
#然后定义了类的若干个基本属性，使用attr_reader(可能还有别的定义方式，也叫attr_啥啥的)
#然后是类的初始化构造器，用来初始化类对象的基本属性。
#然后是类的一个public方法exec，这个方法是一个类中比较特殊的方法，名称含义未知。
#       这个方法是对git-receive-pack这个操作的一个权限判断，返回值为布尔，有两种大结果
        #1.不是异常情况
        #权限被允许，则不报AccessDeniedError的错。
        #权限不被允许，则报AccessDeniedError的错。
        #2.异常情况
        #ApiUnreachableError 返回false
        #AccessDeniedError 返回false
        #看到第一种情况会有可能抛出异常，被下面的异常情况给捕获到。

#然后是类的一个protect方法api
#


require_relative 'gitlab_init'
require_relative 'gitlab_net'
require_relative 'gitlab_access_status'
require_relative 'names_helper'
require 'json'

class GitlabAccess
  class AccessDeniedError < StandardError; end

  include NamesHelper

  attr_reader :config, :repo_path, :repo_name, :changes, :protocol

  def initialize(repo_path, actor, changes, protocol)
    @config = GitlabConfig.new
    @repo_path = repo_path.strip
    @actor = actor
    @repo_name = extract_repo_name(@repo_path.dup)
    @changes = changes.lines
    @protocol = protocol
  end

  def exec
    status = api.check_access('git-receive-pack', @repo_name, @actor, @changes, @protocol)

    raise AccessDeniedError, status.message unless status.allowed?

    true
  rescue GitlabNet::ApiUnreachableError
    $stderr.puts "GitLab: Failed to authorize your Git request: internal API unreachable"
    false
  rescue AccessDeniedError => ex
    $stderr.puts "GitLab: #{ex.message}"
    false
  end

  protected

  def api
    GitlabNet.new
  end
end
